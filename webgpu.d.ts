/**
 * Provides typing for WebGPU types. Stopgap until it's more widely supported in IDEs
 */

declare global {
    interface Navigator {
        gpu: any
    }

    interface GPUBufferUsage {
        VERTEX: number
        INDEX: number,
        UNIFORM: number,
        STORAGE: number,
        COPY_DST: number
    }

    interface GPUShaderStage {
        VERTEX: number
        FRAGMENT: number
        COMPUTE: number
    }

    interface GPUTextureUsageFlags {
    }

    interface GPUTextureUsage {
        RENDER_ATTACHMENT: number
    }

    interface GPUSupportedLimits {
    }

    interface GPUQueue {
        writeBuffer: Function
    }

    interface GPUSupportedFeatures {
    }


    interface GPUAdapter {
        requestDevice: Function
        isFallbackAdapter: Boolean,
        limits: GPUSupportedLimits
    }

    interface GPUBuffer {
        unmap: Function
        getMappedRange: Function
    }

    interface GPUCanvasContext {
        configure: Function
        getCurrentTexture: Function
    }

    interface GPUBufferBindingLayout {
    }

    interface GPUSamplerBindingLayout {
    }

    interface GPUTextureBindingLayout {
    }

    interface GPUStorageTextureLayout {
    }

    interface GPUExternalTextureBindingLayout {
    }

    interface GPUBindGroupLayout {
        binding: number
        visibility: number

        buffer?: GPUBufferBindingLayout
        sampler?: GPUSamplerBindingLayout
        texture?: GPUTextureBindingLayout
        storageTexture?: GPUStorageTextureLayout
        externalTexture?: GPUExternalTextureBindingLayout
    }

    interface GPUBindGroupLayoutEntry {

    }

    interface GPUBindGroup {
    }

    interface GPUBindGroupDescriptor {
    }

    interface GPUBindingResource {
    }


    interface GPUTextureBindingLayout {
        sampleType?: string
        viewDimension?: string
        multisampled?: boolean
    }

    interface GPUSamplerBindingLayout {
        type?: string
    }

    interface GPUBindGroupEntry {
        binding: number
        resource: GPUBindingResource
    }

    interface GPURenderPipeline {
        getBindGroupLayout: Function
    }

    interface GPUPrimitiveState {
    }

    interface GPUVertexState {

    }

    interface GPUDepthStencilState {
    }

    interface GPUMultisampleState {
    }

    interface GPUFragmentState {
    }

    interface GPUPipelineLayout {
    }

    interface GPUTexture {
        createView: Function
        destroy: Function
    }

    interface GPUTextureView {
    }


    interface GPURenderPipelineDescriptor {
        layout: string | GPUPipelineLayout
        vertex: GPUVertexState
        primitive?: GPUPrimitiveState
        depthStencil?: GPUDepthStencilState
        multisample?: GPUMultisampleState
        fragment?: GPUFragmentState
    }

    interface GPURenderPassEncoder {
    }

    interface GPUPrimitiveTopology {
    }

    interface GPUTextureViewDescriptor {}
    interface GPUSamplerDescriptor {}

    interface GPUBindGroupLayoutDescriptor{}
    interface GPUShaderModuleDescriptor {}
    interface GPUDevice {
        features: GPUSupportedFeatures,
        label: string,
        limits: GPUSupportedLimits,
        lost: Promise,
        onuncapturederror: null,
        queue: GPUQueue,
        createBuffer: Function
        createShaderModule: Function
        createBindGroup: Function
        createBindGroupLayout: Function
        createRenderPipeline: Function
        createCommandEncoder: Function
        createTexture: Function
        createPipelineLayout:Function
        createSampler:Function
        createShaderModule:Function
    }


    interface GPUCommandBuffer {
    }

    interface GPURenderPassDescriptor {
    }

    interface GPUQueue {
        submit: Function
    }

    interface GPURenderPassEncoder {
        setPipeline: Function
        draw: Function
        end: Function
    }

    interface GPUCommandEncoder {
        finish: Function
        beginRenderPass: Function
    }

    interface GPUColor {
    }

    interface GPUTextureView {
    }

    interface GPUExtent3D {
    }

    interface GPUTextureFormat {
    }

    interface GPUPipelineLayoutDescriptor {}

    interface GPUTextureDescriptor {
        size: GPUExtent3D
        format: GPUTextureFormat
        usage: GPUTextureUsageFlags
        viewFormats?: Array<GPUTextureFormat>
        mipLevelCount?: number
        sampleCount?: number
        dimension?: string
        label?: string
    }

    interface GPURenderPassColorAttachment {
        view: GPUTextureView
        clearValue: GPUColor
        loadOp: string
        storeOp: string
    }

    interface GPUQuerySet {
    }

    interface GPURenderPassTimestampWrites {
    }

    interface GPURenderPassDepthStencilAttachment {
    }

    interface GPURenderPassDescriptor {
        colorAttachments: Array<GPURenderPassColorAttachment>
        depthStencilAttachment?: GPURenderPassDepthStencilAttachment
        occlusionQuerySet?: GPUQuerySet
        timestampWrites?: GPURenderPassTimestampWrites
        maxDrawCount?: number
    }

    interface Window {
        GPUBufferUsage
        GPUShaderStage
        GPUTextureAttachment
        GPUAdapter
        GPUCanvasContext
        GPUBuffer
        GPUBindGroup
        GPUBindGroupLayout
        GPUBindGroupDescriptor
        GPUBindGroupEntry
        GPUCommandEncoder
        GPUCommandBuffer
        GPURenderPassDescriptor
    }

    //// CUSTOM TYPES /////

    export class ShaderSource {
        vertex: string
        vertexMain: string
        fragment: string
        fragmentMain: string
    }

    interface ShaderModule {
        module: any
        entryPoint: string
        buffers?: Array<any>,
        targets?: Array<any>
    }

    interface ShaderStruct {
        vertex: ShaderModule
        fragment: ShaderModule
    }

    declare module "*?raw" {
        const content: string
        export default content
    }
}


export {}
