WebGPU TS Types
===

A set of types to use when working with WebGPU and Typescript until the types are supported by default in the various IDEs out there.

Setup
===
Just drop into wherever you're storing your types

Notes
===
* Still being flushed out as time permits. Some types may not exist yet
* Note that I don't intend to fully match the spec but just common types that most people will run into